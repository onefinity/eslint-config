import configure from './index.js'; // eslint-disable-line @onefinity/eslint-config/import-grouping

export default configure([{
    rules: {
        '@onefinity/eslint-config/import-grouping': ['error', {
            groups: [{
                matches: /^(?:\w|@\w).*$/.source
            }, {
                label: 'Configurations',
                matches: /\/configurations/.source
            }, {
                label: 'Rules',
                matches: /\/rules/.source
            }, {
                label: 'Helpers',
                matches: /\/helpers/.source
            }, {
                label: 'Constants',
                matches: /\/constants/.source
            }]
        }]
    }
}]);
