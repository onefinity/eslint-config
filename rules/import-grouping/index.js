// Helpers
import { getImportSource, getImportDeclarationType, getGroupLabelFromComment, getGroupLabelFromLine, getGroupLabels, isGroupConfigured, isGroupDuplicate, getImportDeclarationLines, getImportDeclarationGroups, getImportDeclarations, getDuplicateGroups } from './helpers.js';

const DEFAULT_EMPTY_PRECEDING_LINES = 1;
const DEFAULT_GROUPS = [];
const DEFAULT_ORDER = [
    'module', // import 'a';
    'namespace', // import * as B from 'b';
    'default', // import C from 'c';
    'default-and-named', // import D, { D2 } from 'd';
    'named' // import { E } from 'e';
];

export default {
    meta: {
        type: 'layout',
        fixable: 'code',
        messages: {
            importNotAllowedInLabeledGroup: `Importing from '{{ source }}' is not allowed in group '{{ group }}'`,
            importNotAllowedInUnlabeledGroup: `Importing from '{{ source }}' is not allowed in unlabeled group`,
            importOrderIncorrect: `Incorrect import order, {{ current }} imports should be listed before {{ before }} imports`,
            groupNotConfigured: `Group '{{ group }}' is not configured`,
            groupDuplicate: `Group '{{ group }}' is listed multiple times`,
            groupOrderIncorrect: `Incorrect group order, group '{{ current }}' should be listed before group '{{ before }}'`,
            groupWithIncorrectEmptyNewlines: `Group '{{ group }}' is preceded by {{ count }} empty lines, expected {{ expected }}`
        },
        schema: [{
            type: 'object',
            properties: {
                groups: {
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            label: {
                                type: 'string'
                            },
                            matches: {
                                anyOf: [{
                                    type: 'string'
                                }, {
                                    type: 'array',
                                    items: {
                                        type: 'string',
                                        uniqueItems: true
                                    },
                                    additionalItems: true
                                }]
                            }
                        },
                        required: ['matches'],
                        additionalProperties: false
                    }
                },
                order: {
                    type: 'array',
                    items: {
                        type: 'string',
                        enum: DEFAULT_ORDER,
                        uniqueItems: true
                    },
                    additionalItems: false
                }
            },
            required: ['groups'],
            additionalProperties: false
        }]
    },
    create(context) {
        const source = context.getSourceCode();
        const lines = getImportDeclarationLines(source);
        const declaredGroups = getImportDeclarationGroups(source);
        const options = {
            order: DEFAULT_ORDER,
            groups: DEFAULT_GROUPS,
            ...context.options.at(0)
        };

        const incorrectEmptyNewlinesResults = [...lines].reverse().map((line, index, reversedLines) => {
            const label = getGroupLabelFromLine(line);
            const count = reversedLines.slice(index + 1).findIndex((line) => {
                return line.trim() !== '';
            });
            const comment = source.getAllComments().find((comment) => {
                return comment.loc.start.line === lines.length - index;
            });

            if (label === undefined || count === DEFAULT_EMPTY_PRECEDING_LINES || count === -1) {
                return;
            }

            context.report({
                node: comment,
                messageId: 'groupWithIncorrectEmptyNewlines',
                data: {
                    group: label,
                    count,
                    expected: DEFAULT_EMPTY_PRECEDING_LINES
                },
                fix: (fixer) => {
                    const start = source.lineStartIndices[comment.loc.start.line - count - 1];
                    const end = source.lineStartIndices[comment.loc.start.line - 1];

                    return fixer.replaceTextRange([start, end], '\n'.repeat(DEFAULT_EMPTY_PRECEDING_LINES));
                }
            });

            return true;
        });

        const hasGroups = incorrectEmptyNewlinesResults.every((result) => {
            return !result;
        });

        if (hasGroups) {
            for (const [index, declaredGroup] of declaredGroups.entries()) {
                const configuredLabels = getGroupLabels(options.groups);
                const duplicateDeclaredGroups = getDuplicateGroups(declaredGroups, declaredGroup.label);
                const precedingDeclaredGroup = declaredGroups.slice(0, index).find((precedingDeclaredGroup) => {
                    return configuredLabels.indexOf(precedingDeclaredGroup.label) > configuredLabels.indexOf(declaredGroup.label);
                });

                if (precedingDeclaredGroup !== undefined && precedingDeclaredGroup.label !== undefined) {
                    context.report({
                        node: declaredGroup.comment,
                        messageId: 'groupOrderIncorrect',
                        data: {
                            current: declaredGroup.label,
                            before: precedingDeclaredGroup.label
                        },
                        fix: (fixer) => {
                            const text = source.text.slice(...declaredGroup.range);

                            return [
                                fixer.insertTextBeforeRange(precedingDeclaredGroup.range, text),
                                fixer.removeRange(declaredGroup.range)
                            ];
                        }
                    });
                } else if (isGroupDuplicate(duplicateDeclaredGroups, declaredGroup)) {
                    context.report({
                        node: declaredGroup.comment,
                        messageId: 'groupDuplicate',
                        data: {
                            group: declaredGroup.label
                        },
                        fix: (fixer) => {
                            const text = source.text.slice(...declaredGroup.range).slice(declaredGroup.comment.range.at(1) - declaredGroup.range.at(0) + 1);

                            return [
                                fixer.insertTextAfterRange([duplicateDeclaredGroups.at(0).range.at(0), duplicateDeclaredGroups.at(0).range.at(1) - 1], text),
                                fixer.removeRange(declaredGroup.range)
                            ];
                        }
                    });
                } else if (!isGroupConfigured(options.groups, declaredGroup.label)) {
                    context.report({
                        node: declaredGroup.comment,
                        messageId: 'groupNotConfigured',
                        data: {
                            group: declaredGroup.label
                        }
                    });
                }
            }
        }

        return {
            ImportDeclaration: (node) => {
                const comments = source.getTokensBefore(node, {
                    includeComments: true,
                    filter: (token) => { return token.type === 'Line'; }
                });
                const comment = comments.length ? comments.at(-1) : undefined;
                const group = getGroupLabelFromComment(comment);

                if (!isGroupConfigured(options.groups, group)) {
                    return;
                }

                // Check if the import is allowed in the group it's in
                const allowedGroups = options.groups.filter((group) => {
                    const matches = Array.isArray(group.matches) ? group.matches : [group.matches];

                    return matches.some((match) => {
                        const regex = new RegExp(match);

                        return regex.test(getImportSource(node, context));
                    });
                });

                if (!getGroupLabels(allowedGroups).includes(group)) {
                    context.report({
                        node,
                        messageId: group ? 'importNotAllowedInLabeledGroup' : 'importNotAllowedInUnlabeledGroup',
                        data: {
                            source: node.source.value,
                            group: group
                        }
                    });
                }

                // Check if the import should be before any of the previous imports in this group
                const type = getImportDeclarationType(node.specifiers);
                const start = comment ? comment.range.at(1) : 0;
                const end = node.range.at(0);
                const match = getImportDeclarations(node.parent.body).filter((node) => {
                    return node.range.at(0) >= start && node.range.at(0) < end;
                }).find((declaration) => {
                    return options.order.indexOf(getImportDeclarationType(declaration.specifiers)) > options.order.indexOf(type);
                });

                if (match) {
                    context.report({
                        node,
                        messageId: 'importOrderIncorrect',
                        data: {
                            current: type,
                            before: getImportDeclarationType(match.specifiers)
                        },
                        fix: (fixer) => {
                            const nodeLineRange = [
                                source.lineStartIndices[node.loc.start.line - 1],
                                source.lineStartIndices[node.loc.end.line]
                            ];
                            const matchLineRange = [
                                source.lineStartIndices[match.loc.start.line - 1],
                                source.lineStartIndices[match.loc.end.line]
                            ];
                            const nodeLine = source.text.slice(...nodeLineRange);

                            return [
                                fixer.insertTextBeforeRange(matchLineRange, nodeLine),
                                fixer.removeRange(nodeLineRange)
                            ];
                        }
                    });
                }
            }
        };
    }
};
