import { RuleTester } from 'eslint';

// Rules
import rule from './index.js';

const ruleTester = new RuleTester();

const options = [{
    groups: [{
        matches: /^(?:\w|@\w).*$/.source
    }, {
        label: 'Group 1',
        matches: /group-1/.source
    }, {
        label: 'Group 2',
        matches: /group-2/.source
    }]
}];

ruleTester.run('import-grouping', rule, {
    valid: [{
        code: `
            import A from 'package-a';
            import B from 'package-b';
            import C, { C2 } from 'package-c';

            // Group 1
            import D from './group-1/d';

            // Group 2
            import E from './group-2/e';
            import F from './group-2/f';
        `,
        options
    }, {
        code: `
            /* global */
            import A from 'package-a';

            // Group 1
            import D from './group-1/d';
        `,
        options
    }, {
        code: `
            // Group 2
            import A from './group-2/a';
        `,
        options
    }],
    invalid: [{
        code: `
            import A from 'package-a';

            // Group 1
            import B from './group-2/b';
        `,
        options,
        errors: [{
            messageId: 'importNotAllowedInLabeledGroup',
            data: {
                source: './group-2/b',
                group: 'Group 1'
            }
        }]
    }, {
        code: `
            import A from './group-2/a';
        `,
        options,
        errors: [{
            messageId: 'importNotAllowedInUnlabeledGroup',
            data: {
                source: './group-2/a',
                group: 'Group 2'
            }
        }]
    }, {
        code: `
            import * as B from 'b';
            import 'a';
        `,
        options,
        output: `
            import 'a';
            import * as B from 'b';
        `,
        errors: [{
            messageId: 'importOrderIncorrect',
            data: {
                current: 'module',
                before: 'namespace'
            }
        }]
    }, {
        code: `
            import 'a';

            // Group -1
            import 'b';
            import 'c';

            // Non-group comment
        `,
        options,
        errors: [{
            messageId: 'groupNotConfigured',
            data: {
                group: 'Group -1'
            }
        }]
    }, {
        code: `
            import 'a';

            // Group 1
            import './group-1/b';

            // Group 1
            import './group-1/c';

            // Group 2
            import './group-2/d';
        `,
        options,
        output: `
            import 'a';

            // Group 1
            import './group-1/b';
            import './group-1/c';

            // Group 2
            import './group-2/d';
        `,
        errors: [{
            messageId: 'groupDuplicate',
            data: {
                group: 'Group 1'
            }
        }]
    }, {
        code: `
            import 'a';

            // Group 2
            import './group-2/a';

            // Group 1
            import './group-1/a';
        `,
        options,
        output: `
            import 'a';

            // Group 1
            import './group-1/a';

            // Group 2
            import './group-2/a';
        `,
        errors: [{
            messageId: 'groupOrderIncorrect',
            data: {
                current: 'Group 1',
                before: 'Group 2'
            }
        }]
    }, {
        code: `
            import 'a';
            // Group 1
            import './group-1/a';


            // Group 2
            import './group-2/a';
        `,
        options,
        output: `
            import 'a';

            // Group 1
            import './group-1/a';

            // Group 2
            import './group-2/a';
        `,
        errors: [{
            messageId: 'groupWithIncorrectEmptyNewlines',
            data: {
                group: 'Group 1',
                count: 0,
                expected: 1
            }
        }, {
            messageId: 'groupWithIncorrectEmptyNewlines',
            data: {
                group: 'Group 2',
                count: 2,
                expected: 1
            }
        }]
    }]
});
