import path from 'node:path';

export const getImportDeclarationType = (specifiers) => {
    const types = specifiers.map((specifier) => {
        return specifier.type;
    });

    if (types.includes('ImportSpecifier') && types.includes('ImportDefaultSpecifier')) {
        return 'default-and-named';
    }

    if (types.includes('ImportDefaultSpecifier')) {
        return 'default';
    }

    if (types.includes('ImportNamespaceSpecifier')) {
        return 'namespace';
    }

    if (!types.length) {
        return 'module';
    }

    if (types.includes('ImportSpecifier')) {
        return 'named';
    }
};

export const getImportSource = (node, context) => {
    const source = node.source.value;

    if (source.startsWith('.')) {
        return `/${path.relative(context.getCwd(), path.resolve(path.dirname(context.getFilename()), source))}`;
    }

    return source;
};

export const getImportDeclarations = (body) => {
    return body.filter((node) => {
        return node.type === 'ImportDeclaration';
    });
};

export const getImportDeclarationLines = (source) => {
    const declarations = getImportDeclarations(source.ast.body);

    if (!declarations.length) {
        return [];
    }

    return source.lines.slice(0, declarations.at(-1).loc.end.line);
};

export const getImportDeclarationGroups = (source) => {
    return getImportDeclarationLines(source).reduce((declaredGroups, line, index) => {
        if (line.trim().startsWith('//')) {
            const label = getGroupLabelFromLine(line);
            const comment = source.getAllComments().find((comment) => {
                return comment.loc.start.line === index + 1;
            });

            return [...declaredGroups, {
                label,
                comment,
                range: [
                    source.lineStartIndices[comment.loc.start.line - 1] - 1,
                    comment.range.at(1) + 1
                ]
            }];
        }

        if (!declaredGroups.length) {
            return [{
                range: [
                    source.lineStartIndices[index],
                    source.lineStartIndices[index] + line.length + 1
                ]
            }];
        }

        return [...declaredGroups.slice(0, -1), {
            ...declaredGroups.at(-1),
            range: [
                declaredGroups.at(-1).range.at(0),
                declaredGroups.at(-1).range.at(1) + line.length + 1
            ]
        }];
    }, []);
};

export const getGroupLabelFromComment = (comment) => {
    return comment ? comment.value.trim() : undefined;
};

export const getGroupLabelFromLine = (line) => {
    const trimmedLine = line.trim();

    if (!trimmedLine.startsWith('//')) {
        return;
    }

    return trimmedLine.replace(/^\/\//, '').trim();
};

export const getGroupLabels = (groups) => {
    return groups.map((group) => {
        return group.label;
    });
};

export const getDuplicateGroups = (groups, label) => {
    return groups.filter((group) => {
        return group.label === label;
    });
};

export const isGroupConfigured = (groups, label) => {
    if (label === undefined) {
        return true;
    }

    return groups.some((group) => {
        return group.label === label;
    });
};

export const isGroupDuplicate = (groups, group) => {
    return groups.length > 1 && groups.at(0) !== group;
};
