export default {
    meta: {
        fixable: 'code',
        messages: {
            startOfArrayWithObjects: 'There should be no whitespace between the opening bracket of an array and the opening brace of its first object',
            endOfArrayWithObjects: 'There should be no whitespace between the closing bracket of an array and the closing brace of its last object',
            betweenObjectsInArray: 'The closing and opening braces of two consecutive objects in an array should be on the same line'
        }
    },
    create(context) {
        const source = context.getSourceCode();

        return {
            ArrayExpression: (node) => {
                const tokens = source.getTokens(node);

                // Should start with '[{'
                if (tokens.at(0).value === '[' && tokens.at(1).value === '{' && tokens.at(0).range.at(1) !== tokens.at(1).range.at(0)) {
                    context.report({
                        node: node.elements.at(0),
                        messageId: 'startOfArrayWithObjects',
                        fix: (fixer) => {
                            return fixer.removeRange([tokens.at(0).range.at(1), tokens.at(1).range.at(0)]);
                        }
                    });
                }

                // Should end with '}]'
                if (tokens.at(-1).value === ']' && tokens.at(-2).value === '}' && tokens.at(-1).range.at(0) !== tokens.at(-2).range.at(1)) {
                    context.report({
                        node: node.elements.at(-1),
                        messageId: 'endOfArrayWithObjects',
                        fix: (fixer) => {
                            return fixer.removeRange([tokens.at(-2).range.at(1), tokens.at(-1).range.at(0)]);
                        }
                    });
                }

                // Notation between objects should be '}, {'
                for (const [index, token] of tokens.entries()) {
                    if (!tokens[index + 1] || !tokens[index + 2] || token.value !== '}' || tokens[index + 1].value !== ',' || tokens[index + 2].value !== '{') {
                        continue;
                    }

                    if (token.range.at(1) !== tokens[index + 1].range.at(0) || tokens[index + 1].range.at(1) !== tokens[index + 2].range.at(0) - 1) {
                        context.report({
                            node: node.elements.at(-1),
                            messageId: 'betweenObjectsInArray',
                            fix: (fixer) => {
                                return fixer.replaceTextRange([token.range.at(0), tokens[index + 2].range.at(1)], '}, {');
                            }
                        });
                    }
                }
            }
        };
    }
};
