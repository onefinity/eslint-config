import { RuleTester } from 'eslint';

// Rules
import rule from './index.js';

const ruleTester = new RuleTester();

ruleTester.run('consistent-array-of-objects-notation', rule, {
    valid: [{
        code: `
            [{
                a: 1
            }, {
                a: 2
            }]
        `
    }],
    invalid: [{
        code: `
            [
                {
                    a: 1
                },
                {
                    a: 1
                }
            ]
        `,
        output: `
            [{
                    a: 1
                }, {
                    a: 1
                }]
        `,
        errors: [{
            messageId: 'startOfArrayWithObjects'
        }, {
            messageId: 'endOfArrayWithObjects'
        }, {
            messageId: 'betweenObjectsInArray'
        }]
    }, {
        code: `
            [ {
                a: 1
            },{
                a: 1
            }   ]
        `,
        output: `
            [{
                a: 1
            }, {
                a: 1
            }]
        `,
        errors: [{
            messageId: 'startOfArrayWithObjects'
        }, {
            messageId: 'endOfArrayWithObjects'
        }, {
            messageId: 'betweenObjectsInArray'
        }]
    }]
});
