# Onefinity ESLint Config
Generic base configuration for [ESLint][eslint] supporting setups using
[ESNext][esnext], [Node.js][nodejs], [React][react], and
[TypeScript][typescript]. Combine it with
[`@onefinity/stylelint-config`][onefinity-stylelint-config] to lint CSS as well.


## Available configurations
The following configurations are available and can be extended in a
project-specific ESLint configuration, they will extend the default set of rules.
Note that it's possible to mix and match configurations.

- [`node`](./configurations/node.js)  
  Base configuration for projects targeting [Node.js][nodejs].
- [`react`](./configurations/react.js)  
  Additional configuration and rules for projects using [React][react] and JSX.


## Usage
### Installation
Install the package using the following command.

```shell
npm install --save-dev @onefinity/eslint-config
```

### Extending
Use the [`extends`][eslint-extends] feature provided by ESLint in the
project-specific `eslint.config.mjs` configuration file.

```js
import configure, { configs } from '@onefinity/eslint-config';

export default configure([
    configs.node
]);
```

### Scripts
The following scripts can be added to `package.json` to make it easier to start
the linter.

```json
{
    "scripts": {
        "lint:js": "eslint",
        "lint:fix": "npm run lint:js -- --fix"
    }
}
```



[eslint]: https://eslint.org/
[esnext]: https://esnext.github.io/
[nodejs]: https://nodejs.org/
[react]: https://reactjs.org/
[typescript]: https://www.typescriptlang.org/
[eslint-extends]: https://eslint.org/docs/user-guide/configuring#extending-configuration-files
[onefinity-stylelint-config]: http://npmjs.com/package/@onefinity/stylelint-config
