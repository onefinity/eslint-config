import tseslint from 'typescript-eslint';

// Configurations
import node from './configurations/node.js';
import react from './configurations/react.js';
import defaults from './configurations/defaults.js';

export const configs = {
    node,
    react
};

const configure = (...configs) => {
    return tseslint.config.apply(undefined, [defaults, configs].flat());
};

export default configure;
