import node from 'eslint-plugin-n';

// Constants
import { EXTENSIONS } from '../constants.js';

const configs = [{
    rules: {
        'n/no-missing-import': 'off',
        'n/no-missing-require': 'off'
    }
}, {
    files: [`**/test.{${EXTENSIONS.join(',')}}`, `**/test.*.{${EXTENSIONS.join(',')}}`],
    rules: {
        '@typescript-eslint/no-floating-promises': 'off',
        'n/no-unsupported-features/node-builtins': 'off',
        'unicorn/no-null': 'off'
    }
}];

export default [
    node.configs['flat/recommended'],
    ...configs
];
