import globals from 'globals';
import a11y from 'eslint-plugin-jsx-a11y';
import react from 'eslint-plugin-react';
import hooks from 'eslint-plugin-react-hooks';
import imports from 'eslint-plugin-import';

// Constants
import { EXTENSIONS, REACT_EXTENSIONS } from '../constants.js';

export default [{
    files: [`**/*.{${REACT_EXTENSIONS.join(',')}}`],
    ...react.configs.flat['recommended'],
    ...react.configs.flat['jsx-runtime'],
    ...imports.flatConfigs['react'],
    ...a11y.flatConfigs['recommended'],
    languageOptions: {
        ...react.configs.flat.recommended.languageOptions,
        globals: globals.browser
    },
    plugins: {
        'react-hooks': hooks
    },
    rules: {
        ...hooks.configs.recommended.rules,
        '@stylistic/jsx-one-expression-per-line': 'off',
        '@typescript-eslint/no-floating-promises': 'off',
        'react-hooks/exhaustive-deps': 'error'
    }
}, {
    files: [`**/*.{${EXTENSIONS.join(',')}}`],
    rules: {
        '@onefinity/eslint-config/import-grouping': ['error', {
            groups: [{
                matches: /^(?:\w|@\w).*$/.source
            }, {
                label: 'Polyfills',
                matches: /polyfill|modernizr/.source
            }, {
                label: 'Views',
                matches: /\/views/.source
            }, {
                label: 'Containers',
                matches: /\/containers/.source
            }, {
                label: 'Components',
                matches: /\/components/.source
            }, {
                label: 'Store',
                matches: /\/store/.source
            }, {
                label: 'Hooks',
                matches: /\/hooks/.source
            }, {
                label: 'Helpers',
                matches: /\/helpers/.source
            }, {
                label: 'Icons',
                matches: /\/icons/.source
            }, {
                label: 'Images',
                matches: /\/images/.source
            }, {
                label: 'Styles',
                matches: /\.(css|scss|sass)$/.source
            }, {
                label: 'Content',
                matches: /\/content/.source
            }, {
                label: 'Constants',
                matches: /\/constants/.source
            }, {
                label: 'Types',
                matches: /\/types/.source
            }]
        }],
        'unicorn/prevent-abbreviations': ['error', {
            ignore: [
                /^[ij]$/,
                /(prev)?props?/i,
                /(prev)?state?/i,
                /ref/i
            ]
        }]
    }
}];
