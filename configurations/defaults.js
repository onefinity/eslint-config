import path from 'node:path';
import root from 'app-root-path';
import eslint from '@eslint/js';
import globals from 'globals';
import imports from 'eslint-plugin-import';
import unicorn from 'eslint-plugin-unicorn';
import tseslint from 'typescript-eslint';
import comments from '@eslint-community/eslint-plugin-eslint-comments/configs';
import stylistic from '@stylistic/eslint-plugin';

// Rules
import importGroupingRule from '../rules/import-grouping/index.js';
import consistentArrayOfObjectsNotationRule from '../rules/consistent-array-of-objects-notation/index.js';

// Constants
import { EXTENSIONS, JAVASCRIPT_EXTENSIONS } from '../constants.js';

const configs = [{
    settings: {
        'import/resolver': {
            typescript: {
                project: path.resolve(root.path, 'tsconfig.json'),
                alwaysTryTypes: true
            }
        },
        'import/ignore': [
            'copy-webpack-plugin'
        ]
    }
}, {
    languageOptions: {
        parserOptions: {
            projectService: true,
            tsconfigRootDir: root.path
        }
    }
}, {
    ignores: [
        'build/*',
        'coverage/*',
        'node_modules/*'
    ]
}, {
    plugins: {
        '@onefinity/eslint-config': {
            rules: {
                'import-grouping': importGroupingRule,
                'consistent-array-of-objects-notation': consistentArrayOfObjectsNotationRule
            }
        }
    },
    linterOptions: {
        reportUnusedDisableDirectives: true
    }
}, {
    files: [`**/*.{${EXTENSIONS.join(',')}}`],
    rules: {
        '@eslint-community/eslint-comments/no-use': ['error', {
            allow: ['eslint-disable', 'eslint-disable-line', 'eslint-enable']
        }],
        '@onefinity/eslint-config/consistent-array-of-objects-notation': 'error',
        '@onefinity/eslint-config/import-grouping': ['error', {
            groups: [{
                matches: /^(?:\w|@\w).*$/.source
            }, {
                label: 'Helpers',
                matches: /\/helpers/.source
            }, {
                label: 'Constants',
                matches: /\/constants/.source
            }, {
                label: 'Types',
                matches: /\/types/.source
            }]
        }],
        '@stylistic/member-delimiter-style': ['error', {
            multiline: {
                delimiter: 'semi',
                requireLast: true
            },
            singleline: {
                delimiter: 'semi',
                requireLast: true
            }
        }],
        '@stylistic/multiline-ternary': ['error', 'always-multiline', {
            ignoreJSX: true
        }],
        '@typescript-eslint/ban-ts-comment': ['error', {
            'ts-expect-error': 'allow-with-description',
            'minimumDescriptionLength': 10
        }],
        '@typescript-eslint/no-empty-object-type': 'off',
        '@typescript-eslint/no-misused-promises': 'off',
        '@typescript-eslint/no-unnecessary-type-parameters': 'off',
        '@typescript-eslint/no-unsafe-return': 'off',
        '@typescript-eslint/no-unused-vars': ['error', {
            ignoreRestSiblings: true
        }],
        '@typescript-eslint/only-throw-error': 'off',
        '@typescript-eslint/restrict-template-expressions': ['error', {
            allowNumber: true
        }],
        'arrow-body-style': ['error', 'always'],
        'import/newline-after-import': 'error',
        'import/no-duplicates': ['error', {
            'prefer-inline': true
        }],
        'import/no-namespace': 'error',
        'import/no-self-import': 'error',
        'import/no-unresolved': ['error', {
            ignore: [
                /\.scss$/.source,
                /\.svg$/.source
            ]
        }],
        'import/no-webpack-loader-syntax': 'error',
        'no-console': ['error', {
            allow: ['error', 'warn']
        }],
        'object-property-newline': 'error',
        'unicorn/explicit-length-check': 'off',
        'unicorn/no-array-for-each': 'off',
        'unicorn/no-array-reduce': 'off',
        'unicorn/prefer-at': ['error', {
            checkAllIndexAccess: true
        }],
        'unicorn/template-indent': ['error', {
            indent: 4
        }]
    }
}, {
    files: [`**/webpack.config.{${EXTENSIONS.join(',')}}`],
    languageOptions: {
        globals: globals.node
    },
    rules: {
        'unicorn/prefer-module': 'off'
    }
}, {
    files: [`**/*.{${JAVASCRIPT_EXTENSIONS.join(',')}}`],
    extends: [tseslint.configs.disableTypeChecked] // eslint-disable-line import/no-named-as-default-member
}];

export default [
    eslint.configs['recommended'],
    tseslint.configs['strictTypeChecked'], // eslint-disable-line import/no-named-as-default-member
    tseslint.configs['stylisticTypeChecked'], // eslint-disable-line import/no-named-as-default-member
    comments['recommended'],
    imports.flatConfigs['recommended'],
    imports.flatConfigs['typescript'],
    unicorn.configs['flat/recommended'],
    stylistic.configs.customize({
        semi: true,
        indent: 4,
        arrowParens: true,
        braceStyle: '1tbs',
        commaDangle: 'never',
        quoteProps: 'consistent-as-needed'
    }),
    ...configs
];
