export const JAVASCRIPT_EXTENSIONS = [
    'js',
    'jsx',
    'cjs',
    'mjs'
];

export const TYPESCRIPT_EXTENSIONS = [
    'ts',
    'tsx'
];

export const REACT_EXTENSIONS = [
    'jsx',
    'tsx'
];

export const EXTENSIONS = [
    ...JAVASCRIPT_EXTENSIONS,
    ...TYPESCRIPT_EXTENSIONS,
    ...REACT_EXTENSIONS
];
