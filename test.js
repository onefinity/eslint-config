import configure from './index.js'; // eslint-disable-line @onefinity/eslint-config/import-grouping
import { it } from 'node:test';
import { expect } from 'expect';
import { ESLint } from 'eslint';
import { readdir } from 'node:fs/promises';

it('lints without errors', async () => {
    const eslint = new ESLint({
        overrideConfigFile: true,
        baseConfig: configure()
    });
    const results = await eslint.lintText(`import './helpers';`);

    expect(results.at(0).messages).toEqual(expect.arrayContaining([
        expect.objectContaining({
            ruleId: '@onefinity/eslint-config/import-grouping',
            severity: 2
        }),
        expect.objectContaining({
            ruleId: 'import/no-unresolved',
            severity: 2
        }),
        expect.objectContaining({
            ruleId: '@stylistic/eol-last',
            severity: 2
        })
    ]));
});

for (const filename of await readdir('./configurations')) {
    const configuration = await import(`./configurations/${filename}`);
    const eslint = new ESLint({
        overrideConfigFile: true,
        baseConfig: configure([configuration.default])
    });

    expect(eslint.lintText(`import './helpers';`)).resolves.not.toThrow();
}
